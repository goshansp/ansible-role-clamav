# Change Log
This file contains all notable changes to ansible-role-clamav. The RPM's change log is handled seperately as rendered from `templates/clamav.spec.j2`.

## 0.2.0 - 2022-01-28

### Changed
- changed to default on-demand (clamscan)
- on-access made optional
- disabled packaging and development because broken

### Added
- local libvirt vms rhel


## 0.1.0 - 2021-08-02

### Changed
- only handle freshclam when enabled
- changed dev/pkg variables for git tags/commit_id ...
- made all relevant units PartOf=clamav.target

### Added