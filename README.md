# Ansible Role: ClamAV
Installing ClamAV on Fedora 33&34, CentOS/RedHat 7&8. The objective of this role is surpassing feature parity of commercial anti-malware scanners due to clamav's superior security architecture and seamless Linux integration.


## Functional Description
The signatures are updated every thirty minutes and a clamscan is performed daily between 22h00 and 02h00. Positively identified files are logged to /var/log/clamav.log and by default no remediation action is taken. 

SystemD, Rsyslog and ClamAV are beeing leveraged and no further dependencies are required.


## Features
- Out-Of-The-Box malware protection for Linux
- Daily scheduled scan of local filesystem
- Logging to syslog
- PCI-DSS compliant
- WIP: Selinux compatible
- WIP: On-access scanning

## Additional Features
This role comprises of a broad set of functinality like installation of development environment, compilation and packaging / static library inclusion that are not exposed by default.

# Role Variables
[defaults/main.yml](defaults/main.yml)


# Requirements for Development
Tested on Fedora 35 host
```
$ dnf install vagrant libvirt vagrant-libvirt python3-devel -y
$ git clone git@gitlab.com:goshansp/ansible-role-clamav.git
$ python -m venv venv-clamav; source venv-clamav/bin/activate; cd ansible-role-clamav; pip install wheel; pip install --upgrade pip
$ pip install -r requirements.txt
# lower system specs if needed, comment out entire OSes under molecule/defaults/molecule.yml
$ molecule test
```


# EICAR Test
```
$ echo 'K5B!C%@NC[4\CMK54(C^)7PP)7}$RVPNE-FGNAQNEQ-NAGVIVEHF-GRFG-SVYR!$U+U*' | tr '[A-Za-z]' '[N-ZA-Mn-za-m]' > eicar.com
```

# Red Hat Enterprise Linux Subscription
To get access to RHEL base repositories you must provide the following for `generic/rhel` to subscribe
```
export RHEL_SUBSCRIPTION_ORG=
export RHEL_SUBSCRIPTION_ACTIVATIONKEY=
```

# Molecule Scenario default (Scheduled Scan)
The `default` scenario as described above will test against local libvirt boxes and install clamav from rpm (epel), applying and testing `ansible-role-clamav`. The `local-*` scenario will will setup a development environment into local libvirt boxes. Scenarios `cloud-*` differs to `local-*` by spinning up vm instances in the cloud and require `CLOUDSCALE_API_TOKEN` to be set. 

### invocation
```
$ molecule test # for default scenario
$ molecule test -s <scenario_name>
```
## Molecule Scenario local-clamdscan
Deploy role in daily-clamdscan mode.

## Molecule Scenario local-onaccess
Deploy role in active on-access scaner mode.

## Molecule Scenario local-development
This scenario git clones latest `https://github.com/Cisco-Talos/clamav` compiles, runs and tests `clamd` / `clamonacc`.

## Molecule Scenario local-packaging
This scenario packages clamav into an rpm. It may also include a later, static version of libcurl for performance optimization on RHEL7.

## Molecule Scenarios cloud-default, cloud-onaccess, cloud-development, cloud-package
This scenario is used by CI as configured in `.gitlab-ci.yml`


# Example Playbook
```
    - hosts: all
      roles:
         - { role: ansible-role-clamav }
```


# Sequence Diagramm On-Access

```mermaid
sequenceDiagram
    participant clamav.target
    participant freshclam as clamav-freshclam.service
    participant clamonacc as clamav-clamonacc.service
    participant clamd as clamav-clamd.service
    participant clamscan as clamav-clamscan.service 
    
    alt On-Access Mode
        clamav.target->>clamonacc: Start
        clamonacc->>clamd: Start
        clamd->>freshclam: Start
    end

    alt Database Update
        clamav.target->>freshclam: clamav-freshclam.Timer
        freshclam->>clamd: Reload Database
    end

    alt Scheduled Scan
        clamav.target->>clamscan: clamav-clamscan.timer
        clamscan->>clamonacc: Stop
        clamscan->>clamd: Stop
        clamscan->>freshclam: Start
        clamscan->>clamscan: $ clamscan / ...
        clamscan->>clamonacc: Start
        clamonacc->> clamd: Start
    end
```

## WIP: Functional Diagram On-Access
```plantuml
@startuml
package "Environment" {
    node "Local Machine" {
        frame "Unprivileged" {
            [clamav-clamd.service] as clamd
            [clamav-freshclam.service] as freshclam
            database "Local Database"{
                [signatures]
            }
        }
        frame "Privileged" {
            [clamav-clamonacc.service] as clamonacc
            [local_filesystem]
            [/var/spool/quarantine] as quarantine
        }
        [rsyslog]
        database "Log Destination"{
            [/var/log/clamav.log] as log
        }
    }
    database "Database Mirror"{
        [signature_mirror]
    }
}
clamd ---> signatures : load signatures
clamonacc --> quarantine : move file
clamonacc --> clamd : file streaming
local_filesystem --> clamonacc : inotify event
freshclam --> signature_mirror : get signatures
freshclam --> signatures : persist signatures
freshclam --> clamd : reload signatures
rsyslog --> log: log events
@enduml
```

# Stop / Disable ClamAV
```
$ sudo systemctl stop clamav.target; sudo systemctl disable clamav.target
```

# Start / Enable ClamAV
```
$ sudo systemctl stop clamav.target; sudo systemctl disable clamav.target
```


# On-Access Performance Optimization / Tailoring
`clamav_onaccess_exclude_additional_directories` can be used to exclude (incompatible application) paths which cause exessive load via clamd. Should CPU utilization increase the following procedure will help you identifying excessive file access.

```
$ systemctl stop clamav-clamonacc.serice
$ /usr/sbin/clamonacc -F -c /etc/clamd.d/scan.conf --move=/var/spool/quarantine --fdpass --verbose
# poke stuff
$ cat /var/log/clamav.log | grep performing | cut -d ':' -f 5 | sort | uniq -c | sort -nr | head -n10
$ systemctl stop clamav-clamonacc.serice
```


# License

BSD


# Contributors License Agreement

By contributing you agree that these contributions are your own (or approved by your employer) and you grant a full, complete, irrevocable copyright license to all users and developers of the project, present and future, pursuant to the license of the project.


# Author Information

This role was created in 2021 by Hanspeter Gosteli.
